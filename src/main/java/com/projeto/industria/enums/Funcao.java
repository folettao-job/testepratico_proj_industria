package main.java.com.projeto.industria.enums;

public enum Funcao {

    Coordenador,
    Contador,
    Diretor,
    Eletricista,
    Gerente,
    Operador,
    Recepcionaista;
}
