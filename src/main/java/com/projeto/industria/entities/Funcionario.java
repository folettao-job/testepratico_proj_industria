package main.java.com.projeto.industria.entities;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.Objects;

import static main.java.com.projeto.industria.util.FuncionarioUtil.df;

public class Funcionario extends Pessoa {

    private BigDecimal salario;
    private String funcao;

    public Funcionario() {
    }

    public Funcionario(String nome, LocalDate dataNascimento, BigDecimal salario, String funcao) {
        super(nome, dataNascimento);
        this.salario = salario;
        this.funcao = funcao;
    }

    public BigDecimal getSalario() {
        return salario;
    }

    public void setSalario(BigDecimal salario) {
        this.salario = salario;
    }

    public String getFuncao() {
        return funcao;
    }

    public void setFuncao(String funcao) {
        this.funcao = funcao;
    }

    public void aumentoSalario(double aumento) {
        setSalario(getSalario().multiply(new BigDecimal(aumento)));
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), getSalario(), getFuncao());
    }

    @Override
    public String toString() {
        // 3.4
        final StringBuilder sb = new StringBuilder("Funcionário {");
        sb.append("salário=").append(df.format(salario));
        sb.append(", função='").append(funcao).append('\'');
        sb.append(", ").append(super.toString());
        sb.append('}');
        return sb.toString();
    }
}
