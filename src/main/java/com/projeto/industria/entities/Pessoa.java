package main.java.com.projeto.industria.entities;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.Objects;

public class Pessoa {

    private String nome;
    private LocalDate dataNascimento;

    public Pessoa() {
    }

    public Pessoa(String nome, LocalDate dataNascimento) {
        this.nome = nome;
        this.dataNascimento = dataNascimento;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public LocalDate getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(LocalDate dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getNome(), getDataNascimento());
    }

    @Override
    public String toString() {
        // 3.4
        DateTimeFormatter tf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        final StringBuilder sb = new StringBuilder("Pessoa {");
        sb.append("nome='").append(nome).append('\'');
        sb.append(", data de nascimento=").append(tf.format(dataNascimento));
        sb.append('}');
        return sb.toString();
    }
}
