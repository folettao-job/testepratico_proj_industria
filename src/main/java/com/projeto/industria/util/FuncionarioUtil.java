package main.java.com.projeto.industria.util;

import main.java.com.projeto.industria.entities.Funcionario;
import main.java.com.projeto.industria.entities.Pessoa;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.*;

public class FuncionarioUtil {

    public static DecimalFormat df = new DecimalFormat("R$ ###,###,###,##0.00",
            new DecimalFormatSymbols(new Locale("pt", "BR"))
    );

    public static List<Funcionario> aniversarios(List<Funcionario> listFunc, int startMonth, int endMonth) {
        List<Funcionario> bufferFunc = new ArrayList<>();
        bufferFunc.addAll(listFunc);
        bufferFunc.removeIf(x -> x.getDataNascimento().getMonth().getValue() < startMonth ||
                x.getDataNascimento().getMonth().getValue() > endMonth);
        return listFunc;
    }

    public static Pessoa maiorIdade(List<Funcionario> listFunc) {
        Pessoa pessoa = new Pessoa();

        for (Funcionario bufferFunc : listFunc) {
            if (pessoa.getNome() != null) {
                long idade = ChronoUnit.DAYS.between(bufferFunc.getDataNascimento(), LocalDate.now());
                if (idade > ChronoUnit.DAYS.between(pessoa.getDataNascimento(), LocalDate.now())){
                    pessoa.setNome(bufferFunc.getNome());
                    pessoa.setDataNascimento(bufferFunc.getDataNascimento());
                }
            } else {
                pessoa.setNome(bufferFunc.getNome());
                pessoa.setDataNascimento(bufferFunc.getDataNascimento());
            }
        }
        return pessoa;
    }

    public static String totalSalarios(List<Funcionario> listFunc) {
        BigDecimal total = BigDecimal.ZERO;
        for (Funcionario func : listFunc)
            total = total.add(func.getSalario());
        return df.format(total.doubleValue());
    }

    public static Map<String, String> salarioMinimo(List<Funcionario> listFunc, double salMinimo) {
        DecimalFormat df = new DecimalFormat("##0.00");
        Map<String, String> map = new LinkedHashMap<>();
        for (Funcionario func : listFunc)
            map.put(func.getNome(), df.format(func.getSalario().doubleValue() / salMinimo));
        return map;
    }
}
