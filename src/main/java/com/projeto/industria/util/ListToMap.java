package main.java.com.projeto.industria.util;

import main.java.com.projeto.industria.entities.Funcionario;
import main.java.com.projeto.industria.enums.Funcao;

import java.util.*;

public class ListToMap {

    public Map<String, List<Funcionario>> convert(List<Funcionario> listFunc) {
        Map<String, List<Funcionario>> map = new TreeMap<>();
        List<Funcionario> bufferFunc = new ArrayList<>();

        for (Funcao funcao : Arrays.stream(Funcao.values()).toList()) {
            bufferFunc.addAll(listFunc);
            bufferFunc.removeIf(x -> !x.getFuncao().equals(funcao.name()));
            map.put(funcao.name(), bufferFunc);
            bufferFunc = new ArrayList<>();
        }
        return map;
    }
}
