package main.java;

import main.java.com.projeto.industria.entities.Funcionario;
import main.java.com.projeto.industria.enums.Funcao;
import main.java.com.projeto.industria.util.FuncionarioUtil;
import main.java.com.projeto.industria.util.ListToMap;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

public class Principal {
    public static void main(String[] args) {

        List<Funcionario> listFunc = new ArrayList<Funcionario>();

        // 3.1 INSERT BLOCK
        listFunc.add(new Funcionario("Maria", LocalDate.of(2000, 10, 18),
                        new BigDecimal("2009.44"), Funcao.Operador.name()
                )
        );
        listFunc.add(new Funcionario("João", LocalDate.of(1990, 5, 12),
                        new BigDecimal("2284.38"), Funcao.Operador.name()
                )
        );
        listFunc.add(new Funcionario("Caio", LocalDate.of(1961, 5, 2),
                        new BigDecimal("9836.14"), Funcao.Coordenador.name()
                )
        );
        listFunc.add(new Funcionario("Miguel", LocalDate.of(1988, 10, 14),
                        new BigDecimal("19119.88"), Funcao.Diretor.name()
                )
        );
        listFunc.add(new Funcionario("Alice", LocalDate.of(1995, 1, 5),
                        new BigDecimal("2234.68"), Funcao.Recepcionaista.name()
                )
        );
        listFunc.add(new Funcionario("Heitor", LocalDate.of(1999, 11, 19),
                        new BigDecimal("1582.72"), Funcao.Operador.name()
                )
        );
        listFunc.add(new Funcionario("Arthur", LocalDate.of(1993, 3, 31),
                        new BigDecimal("4071.84"), Funcao.Contador.name()
                )
        );
        listFunc.add(new Funcionario("Laura", LocalDate.of(1994, 7, 8),
                        new BigDecimal("3017.45"), Funcao.Gerente.name()
                )
        );
        listFunc.add(new Funcionario("Heloísa", LocalDate.of(2003, 5, 24),
                        new BigDecimal("1606.85"), Funcao.Eletricista.name()
                )
        );
        listFunc.add(new Funcionario("Helena", LocalDate.of(1996, 9, 2),
                        new BigDecimal("2799.93"), Funcao.Gerente.name()
                )
        );

        System.out.println("\n*** Lista de Funcionários Completa ***");
        listFunc.forEach(System.out::println);
        System.out.println("\n");


        // 3.2 DELETE BLOCK
        String nomeRemover = "João";
        if (listFunc.removeIf(x -> x.getNome().equals(nomeRemover))) {
            System.out.printf("\n*** Lista de Funcionários: após Exclusão do usuário: %s***\n", nomeRemover);
            listFunc.forEach(System.out::println);
        } else
            System.err.println("Nome não localizado na lista");
        System.out.println("\n");


        // 3.4 10% increase in salary
        double aumento = 1.1;
        listFunc.forEach(x -> x.aumentoSalario(aumento));
        System.out.println("\n*** Lista de Funcionários: 10% de Aumento Salarial ***");
        listFunc.forEach(System.out::println);
        System.out.println("\n");

        // 3.5 Group by function
        var mapFuncao = new ListToMap().convert(listFunc);
        // 3.6
        System.out.println("\n*** Lista de Funcionários: Agrupado por Função ***");
        mapFuncao.forEach((key, value) -> System.out.println(key + ": " + value));
        System.out.println("\n");

        // 3.7
        System.out.println("\n*** Lista de Funcionários: Aniversariantes ***");
        List<Funcionario> aniversariantes = FuncionarioUtil.aniversarios(listFunc, 10, 12);
        aniversariantes.forEach(System.out::println);
        System.out.println("\n");

        // 3.8
        System.out.println("\n*** Lista de Funcionários: com Maior Idade ***");
        System.out.println(FuncionarioUtil.maiorIdade(listFunc));
        System.out.println("\n");

        // 3.9
        System.out.println("\n*** Lista de Funcionários: lista em Ordem Alfabética ***");
        listFunc.sort((f1, f2) -> f1.getNome().compareTo(f2.getNome()));
        listFunc.forEach(System.out::println);
        System.out.println("\n");

        // 3.10
        System.out.println("\n*** Lista de Funcionários: Total de Salários Somados ***");
        System.out.println(FuncionarioUtil.totalSalarios(listFunc));
        System.out.println("\n");

        // 3.11
        System.out.println("\n*** Lista de Funcionários: Salários Mínimos Recebido por Funcionário ***");
        double salarioMinimo = 1212.00;
        FuncionarioUtil.salarioMinimo(listFunc, salarioMinimo).forEach(
                (key, value) -> System.out.println(key + ": " + value)
        );
    }
}